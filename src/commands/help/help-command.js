const sendMessage = require('../../utils/send-message')

const init = (client) => {
    client.on('messageCreate', async (message) => {
        if (message.content === '!help') {
            sendMessage(message.channel, '<https://gitlab.com/akw5013/discord-race-bot/blob/master/HELP.md>')
        }
    })
}
module.exports = {
    init,
}
