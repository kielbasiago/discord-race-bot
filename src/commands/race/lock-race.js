const saveRace = require('../../db/save-race')
const CONSTANTS = require('../../constants')
const getRaceByKey = require('../../db/race/get-race-by-key')
const updateRaceMessages = require('../../utils/race/update-race-messages')

module.exports = async (message, args, hasAdmin) => {
    if (!message.guild) { return null }
    const race = await getRaceByKey(message.channel.name)
    if (!race) {
        return 'There is no race happening in this channel.'
    }
    if (race.async) {
        return 'This race is an async race, and cannot be locked.'
    }
    const hasPermission = (hasAdmin || race.creator.id === message.author.id)
    if (hasPermission) {
        if (race.status === CONSTANTS.RACE_STATUS.LOCKED) {
            return 'The race is already locked.'
        }
        if (race.status !== CONSTANTS.RACE_STATUS.OPEN) {
            return `You cannot lock the race in the ${race.status} status.`
        }
        race.status = CONSTANTS.RACE_STATUS.LOCKED
        updateRaceMessages(message.client, race)
        await saveRace(race, { status: race.status })
    } else {
        return 'You do not have the proper permissions for this command.'
    }
    return 'The race has been locked.'
}
