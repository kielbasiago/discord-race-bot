const utils = require('./race-utils')
const getRaceByKey = require('../../db/race/get-race-by-key')
const securityUtils = require('../../utils/security/security-utils')

const closeRace = async (message, args, hasAdmin) => {
    if (!message.guild) { return null }
    const race = await getRaceByKey(message.channel.name)
    if (!race) {
        return 'There is no race happening in this channel.'
    }
    const securityResult = securityUtils.verifyRaceMessage(message, race)
    if (securityResult) {
        return securityResult
    }
    const hasPermission = securityUtils.isSentByCreatorOrAdmin(message, race, hasAdmin)
    if (hasPermission) {
        await utils.closeRace(message)
    } else {
        return 'You do not have the proper permissions for this command.'
    }
    return null
}
module.exports = closeRace
