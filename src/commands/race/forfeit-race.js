const moment = require('moment')
const utils = require('./race-utils')
const saveEntrant = require('../../db/race/save-entrant')
const CONSTANTS = require('../../constants')
const grantAccessToSpoilerChannels = require('../../utils/race/grant-access-to-spoiler-channels')
const securityUtils = require('../../utils/security/security-utils')
const getUserDisplayName = require('../../utils/user/get-user-display-name')
const getUserDetails = require('../../utils/user/get-user-details')
const addEntrantToRace = require('../../db/race/add-entrant-to-race')
const getActiveRaceForUser = require('../../utils/user/get-active-race-for-user')
const sendMessage = require('../../utils/send-message')
const sendDirectMessage = require('../../utils/send-direct-message')

const forfeitRace = async (message, args) => {
    let currentRace = null
    let { channel } = message
    const { author: user } = message
    if (!message.guild) {
        currentRace = await getActiveRaceForUser(user)
        if (!currentRace) {
            return 'You do not currently have a race active that can be finished.'
        }
        const guild = await message.client.guilds.fetch(currentRace.guild.id)
        if (!guild) {
            return 'Unable to determine the server this race is running on.  Please consult an admin.'
        }
        channel = guild.channels.cache.find((ch) => ch.name === currentRace.key)
        if (!channel) {
            return 'No channel was found for the race.  Please consult an admin.'
        }
    } else {
        currentRace = await utils.getCurrentRace(channel.name)
    }
    if (!currentRace) { return null }
    if (message.guild && !securityUtils.isMessageOnRaceServer(message, currentRace)) { return `You must send this message on ${currentRace.guild.name}.` }

    let result = null
    let entrant = utils.findEntrant(currentRace, user)
    if (!entrant && !currentRace.async) {
        return `${user.username} is not currently in this race.`
    }
    if (currentRace.status !== CONSTANTS.RACE_STATUS.RUNNING) {
        return `You cannot forfeit from a race in the ${currentRace.status} state.`
    }
    if (!entrant) {
        const userDetails = await getUserDetails(message)
        const displayName = await getUserDisplayName(userDetails, message.guild)
        const teamIndex = args.indexOf('--team')
        const teamName = teamIndex >= 0 ? args[teamIndex + 1] : displayName
        entrant = {
            name: displayName, status: CONSTANTS.ENTRANT_STATUS.RUNNING, id: message.author.id, team: teamName,
        }
        await addEntrantToRace(currentRace, entrant)
    }
    if (entrant.status === CONSTANTS.ENTRANT_STATUS.FORFEIT) {
        return `${user.username} has already forfeited this race.`
    }
    if (entrant.status !== CONSTANTS.ENTRANT_STATUS.RUNNING) {
        return `${user.username} cannot forfeit a race because they are in the ${entrant.status} status.`
    }
    entrant.status = CONSTANTS.ENTRANT_STATUS.FORFEIT
    entrant.finishTime = moment().toISOString(true)
    entrant.finish = CONSTANTS.ENTRANT_STATUS.FORFEIT
    result = `${user.username} has forfeited from the race.`
    grantAccessToSpoilerChannels(message, currentRace)
    saveEntrant(currentRace, entrant)
    utils.checkIfRaceIsOver(channel, currentRace)
    if (!message.guild) {
        sendMessage(channel, result)
        return `You have forfeited from ${currentRace.key}`
    }
    if (currentRace.async) {
        sendDirectMessage(message.author, `You have forfeited from ${currentRace.key}`)
        message.delete()
        return ''
    }
    return result
}
module.exports = forfeitRace
