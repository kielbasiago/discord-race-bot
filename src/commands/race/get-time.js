const moment = require('moment')
const utils = require('./race-utils')
const determineTime = require('../../utils/determine-time')

module.exports = async (message) => {
    if (!message.guild) { return null }
    const currentRace = await utils.getCurrentRace(message.channel.name)
    if (!currentRace) { return null }
    if (!currentRace.startTime) {
        return 'The race has not started yet.'
    }
    const currentTime = determineTime(currentRace.startTime, moment().toISOString(true))
    return `The race has been running for ${currentTime}`
}
