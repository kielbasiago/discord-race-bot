const utils = require('./race-utils')
const saveEntrant = require('../../db/race/save-entrant')
const CONSTANTS = require('../../constants')
const securityUtils = require('../../utils/security/security-utils')
const isRaceStartable = require('../../utils/race/is-race-startable')

const switchTeam = async (message, args) => {
    const currentRace = await utils.getCurrentRace(message.channel.name)
    if (!currentRace || !args || args.length < 1) { return null }
    const securityResult = securityUtils.verifyRaceMessage(message, currentRace)
    if (securityResult) {
        return securityResult
    }
    const [teamName] = args
    const entrant = await utils.findEntrant(currentRace, message.author)
    let result = null
    if (entrant && entrant.status !== CONSTANTS.ENTRANT_STATUS.READY && (isRaceStartable(currentRace) || currentRace.async)) {
        entrant.team = teamName
        result = `${message.author.username} has joined team ${teamName}!`
        saveEntrant(currentRace, entrant)
    } else {
        result = `${message.author.username} is not currently allowed to change teams`
    }
    return result
}
module.exports = switchTeam
