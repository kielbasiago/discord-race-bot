const utils = require('./race-utils')
const CONSTANTS = require('../../constants')
const saveEntrant = require('../../db/race/save-entrant')
const securityUtils = require('../../utils/security/security-utils')

const unforfeitRace = async (message) => {
    const user = message.author
    const race = await utils.getCurrentRace(message.channel.name)
    if (!race) { return null }
    const securityResult = securityUtils.verifyRaceMessage(message, race)
    if (securityResult) {
        return securityResult
    }
    if (race.status !== CONSTANTS.RACE_STATUS.RUNNING) {
        return 'You cannot unforfeit this race, as it has not started or has already finished.'
    }
    const entrant = utils.findEntrant(race, user)
    if (!entrant) {
        return 'You are not currently in this race.'
    }
    if (entrant.status === CONSTANTS.ENTRANT_STATUS.FINISHED) {
        return 'You cannot unforfeit this race. Please use !undone instead'
    }
    if (entrant.status !== CONSTANTS.ENTRANT_STATUS.FORFEIT) {
        return 'You have not forfeited this race.'
    }
    entrant.status = CONSTANTS.ENTRANT_STATUS.RUNNING
    entrant.finishTime = null
    entrant.finish = null
    saveEntrant(race, entrant)
    return `${user.username} has cancelled their forfeit!`
}
module.exports = unforfeitRace
