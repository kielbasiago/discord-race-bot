const finish = require('./finish-race')
const forfeit = require('./forfeit-race')
const join = require('./join-race')
const quit = require('./quit-race')
const ready = require('./ready-for-race')
const create = require('./create-race')
const entrants = require('./list-entrants')
const getRaceDetails = require('./get-race-details')
const unready = require('./unready-for-race')
const closeRace = require('./close-race')
const switchTeams = require('./switch-teams')
const assignTeams = require('./assign-teams')
const addComment = require('./add-comment')
const showRaces = require('./show-races')
const watchRace = require('./watch-race')
const unfinishRace = require('./unfinish-race')
const unforfeitRace = require('./unforfeit-race')
const multiStreamLink = require('./get-multi-link')
const getTime = require('./get-time')
const clearTeam = require('./clear-team')
const setGameMode = require('./set-game-mode')
const setMetadata = require('./set-metadata-value')
const startAsyncRace = require('./start-async-race')
const finishAsyncRace = require('./finish-async-race')
const forceField = require('./force-field')
const wagerCookies = require('./wager-cookies')
const kickUser = require('./kick-user')
const lockRace = require('./lock-race')
const unlockRace = require('./unlock-race')
const setOfficial = require('./set-official')
const setUnofficial = require('./set-unofficial')
const checkForCompletion = require('./check-for-finish')
const donateCookies = require('./donate-cookies')
const setRaceEvent = require('./set-race-event')

module.exports = {
    '!startrace': create,
    '!done': finish,
    '.done': finish, // Because so many people will be used to it already from SRL
    '!forfeit': forfeit,
    '!ff': forfeit,
    '!unforfeit': unforfeitRace,
    '!join': join,
    '!enter': join, // Grumble
    '!quit': quit,
    '!ready': ready,
    '!unready': unready,
    '!entrants': entrants,
    '!close': closeRace,
    '!details': getRaceDetails,
    '!setteam': switchTeams,
    '!randomteams': assignTeams,
    '!comment': addComment,
    '.comment': addComment,
    '!showraces': showRaces,
    '!watch': watchRace,
    '!undone': unfinishRace,
    '.undone': unfinishRace,
    '!multi': multiStreamLink,
    '!clearteam': clearTeam,
    '!time': getTime,
    '!setmode': setGameMode,
    '!setmetadata': setMetadata,
    '!startasync': startAsyncRace,
    '!finishasync': finishAsyncRace,
    '!forcevalue': forceField,
    '!wager': wagerCookies,
    '!kick': kickUser,
    '!lock': lockRace,
    '!unlock': unlockRace,
    '!official': setOfficial,
    '!unofficial': setUnofficial,
    '!forcecheckfinish': checkForCompletion,
    '!donatecookies': donateCookies,
    '!setevent': setRaceEvent,
}
