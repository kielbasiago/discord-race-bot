const setConfigs = require('./set-configs')
const showConfig = require('./show-config')
const toggleFeature = require('./toggle-feature')
const showFeatures = require('./show-features')
const whatsNew = require('./whats-new')
const toggleAlertRole = require('./toggle-race-alert-role')
const donate = require('./donate')

module.exports = {
    '!setracecategory': setConfigs.setRaceCategory,
    '!setmaxvoice': setConfigs.setMaxVoiceForAdmin,
    '!setmaxraces': setConfigs.setMaxRacesForAdmin,
    '!setbotadmin': setConfigs.setBotAdminRole,
    '!setbotchannel': setConfigs.setMainBotChannel,
    '!setbotrole': setConfigs.setMainBotRole,
    '!setracealertchannel': setConfigs.setRaceAlertChannel,
    '!setasyncracecategory': setConfigs.setAsyncRaceCategory,
    '!setracealertrole': setConfigs.setRaceAlertRole,
    '!showconfig': showConfig,
    '!togglefeature': toggleFeature,
    '!showfeatures': showFeatures,
    '!whatsnew': whatsNew,
    '!toggleracealertrole': toggleAlertRole,
    '!donate': donate,
    '!patreon': donate,
}
