const getConfig = require('../../db/get-server-config-by-id')

const showConfig = async (message, args, hasAdmin) => {
    if (!hasAdmin) {
        return 'You do not have permission to do that.'
    }
    const config = await getConfig(message.guild.id)
    if (!config) {
        return 'No config was found for this server.'
    }
    const configToDisplay = { ...config }
    // Don't show the settings
    if (configToDisplay.settings) {
        delete configToDisplay.settings
    }
    if (configToDisplay.createTime) {
        delete configToDisplay.createTime
    }
    return `\`\`\`\n${Object.keys(configToDisplay).filter((key) => key !== 'id').map((key) => `${key} : ${configToDisplay[key]}`).join('\n')}\n\`\`\``
}
module.exports = showConfig
