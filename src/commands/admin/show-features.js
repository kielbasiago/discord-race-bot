const getConfigValue = require('../../utils/get-config-value')
const CONSTANTS = require('../../constants')
const displayTable = require('../../utils/table-layout')

module.exports = async (message, args, hasAdmin) => {
    if (!hasAdmin) { return null }
    if (!message.guild) { return null }

    const headers = ['Feature', 'Enabled?']
    const promises = Object.keys(CONSTANTS.ACTIVE_FEATURES.FEATURES).map(async (key) => {
        const val = await getConfigValue(message, key, CONSTANTS.ACTIVE_FEATURES.FEATURES[key].defaultValue)
        return { key, val }
    })
    const data = await Promise.all(promises)
    return `\`\`\`\n${displayTable(headers, data)}\n\`\`\``
}
