const getRandomNumber = require('../../../utils/get-random-number')
const utils = require('../../race/race-utils')
const saveRace = require('../../../db/save-race')

module.exports = async (message, args) => {
    let maxAscension = 20
    let minAscension = 0
    if (args.includes('--max')) {
        maxAscension = Number(args[args.indexOf('--max') + 1]) || 20
    }
    if (args.includes('--min')) {
        minAscension = Number(args[args.indexOf('--min') + 1]) || 0
    }
    const ascension = getRandomNumber(minAscension, maxAscension)
    const activeRace = await utils.getCurrentRace(message.channel.name)
    if (activeRace) {
        activeRace.metadata = Object.assign(
            activeRace.metadata || {},
            {
                Ascension: ascension,
            },
        )
        message.channel.setTopic(`Ascension ${ascension}`)
        await saveRace(activeRace, { metadata: activeRace.metadata })
    }
    return `${message.author.username} has ascended ${ascension} times! Or something...`
}
