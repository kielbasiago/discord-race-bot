const chooseWeapon = require('./choose-weapon')
const chooseHeat = require('./choose-heat')
const rollHades = require('./roll-hades')

module.exports = {
    '!weapon': chooseWeapon,
    '!hadesheat': chooseHeat,
    '!rollhades': rollHades,
}
