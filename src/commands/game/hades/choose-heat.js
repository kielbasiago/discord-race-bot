const getRandomNumber = require('../../../utils/get-random-number')
const utils = require('../../race/race-utils')
const saveRace = require('../../../db/save-race')

module.exports = async (message, args) => {
    let maxHeat = 32
    let minHeat = 1
    if (args.includes('--maxheat')) {
        maxHeat = Number(args[args.indexOf('--maxheat') + 1]) || 32
    }
    if (args.includes('--minheat')) {
        minHeat = Number(args[args.indexOf('--minheat') + 1]) || 0
    }
    const heat = getRandomNumber(minHeat, maxHeat)
    const activeRace = await utils.getCurrentRace(message.channel.name)
    if (activeRace) {
        activeRace.metadata = Object.assign(
            activeRace.metadata || {},
            {
                Heat: heat,
            },
        )
        await saveRace(activeRace, { metadata: activeRace.metadata })
    }
    return `${message.author.username} turns the heat to ${heat} !`
}
