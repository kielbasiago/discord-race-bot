const chooseHeat = require('./choose-heat')
const chooseWeapon = require('./choose-weapon')
const utils = require('../../race/race-utils')
const saveRace = require('../../../db/save-race')

module.exports = async (message, args) => {
    const weapon = await chooseWeapon(message, args)
    const heat = await chooseHeat(message, args)
    const activeRace = await utils.getCurrentRace(message.channel.name)
    if (activeRace) {
        activeRace.metadata = Object.assign(
            activeRace.metadata || {},
            {
                Roller: message.author.username,
            },
        )
        await saveRace(activeRace, { metadata: activeRace.metadata })
    }
    return `Selected Weapon: ${weapon}\n${heat}`
}
