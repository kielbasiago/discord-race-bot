const ff4flags = require('./ff4flags')
const ff4beta = require('./ff4beta')
const zz5blue = require('./zz5blue')
const zz5red = require('./zz5red')

module.exports = {
    '!ff4flags': ff4flags,
    '!ff4beta': ff4beta,
    '!zz5blue': zz5blue,
    '!zz5red': zz5red,
}
