const baseUrl = 'http://ff4fe.com/api'
const fetch = require('node-fetch')
const randomstring = require('randomstring')
const sendMessage = require('../../../utils/send-message')
const utils = require('../../race/race-utils')
const logger = require('../../../utils/logger')
const saveRace = require('../../../db/save-race')
const securityUtils = require('../../../utils/security/security-utils')
const isRaceEditable = require('../../../utils/race/is-race-editable')

const MAX_TRIES = 3
const ff4apikey = process.env.FE_API_KEY
const SEED_LENGTH = 10
const setFlags = (flagString) => {
    const seed = randomstring.generate(SEED_LENGTH).toUpperCase()
    return {
        seed,
        flags: flagString,
    }
}
const updateRaceDetails = async (message, seedDetails, hasAdmin, hiddenFlags = false) => {
    if (!message || !message.channel) { return }
    const currentRace = await utils.getCurrentRace(message.channel.name)
    if (!currentRace) { return }
    if (currentRace.official && !hasAdmin) {
        message.channel.send('Only admins can update the race seed.  Please do not use this seed for the race.')
        return
    }
    if (!isRaceEditable(currentRace) && !hasAdmin) {
        message.channel.send('The flags cannot be updated once the race has been started.  Please do not use this seed for the race.')
        return
    }
    if (!securityUtils.isMessageOnRaceServer(message, currentRace)) {
        message.channel.send(`The flags can only be updated on ${currentRace.guild.name}. Please do not use this seed for the race.`)
        return
    }
    const metadata = {
        Version: seedDetails.version,
        Seed: seedDetails.seed,
        Flags: hiddenFlags ? '<hidden>' : seedDetails.flags,
        Url: seedDetails.url,
        Hash: seedDetails.verification,
        Roller: message.author.username,
    }
    if (currentRace.metadata) {
        currentRace.metadata = Object.assign(currentRace.metadata, metadata)
    } else {
        currentRace.metadata = metadata
    }
    message.channel.setTopic(`${seedDetails.url} : ${seedDetails.verification}`)
    saveRace(currentRace, { metadata: currentRace.metadata })
}
module.exports = async (message, args, hasAdmin, url = baseUrl) => {
    if (!args || args.length === 0) {
        return 'No flags were given.'
    }
    let hidden = false
    if (args.includes('--hidden')) {
        hidden = true
    }
    const fullFlagset = args.join(' ').replace('--hidden', '').trim()
    const flagInfo = setFlags(fullFlagset)
    let taskId = null
    let seedId = null
    const generationUrl = `${url}/generate?key=${ff4apikey}`
    const postBody = {
        flags: flagInfo.flags,
        seed: flagInfo.seed,
    }
    if (hidden) {
        postBody.metaconfig = { hide_flags: true }
        if (message.guild) {
            message.delete()
        }
    }
    let tries = 0
    fetch(generationUrl, {
        method: 'post',
        body: JSON.stringify(postBody),
    })
        .then((res) => res.json())
        .then((json) => {
            if (json.status === 'ok') {
                taskId = json.task_id
            } else if (json.status === 'exists') {
                seedId = json.seed_id
            } else {
                return json
            }
            return null
        })
        .then((error) => {
            if (error) {
                sendMessage(message.channel, `Error generating seed: ${error.error}`)
            }
            if (taskId) {
                let processing = false
                const poll = setInterval(() => {
                    const detailsUrl = `${url}/task?key=${ff4apikey}&id=${taskId}`
                    if (!processing) {
                        if (tries > MAX_TRIES) {
                            clearInterval(poll)
                            sendMessage(message.channel, 'Maximum attempts reached. No longer polling for seed details.')
                        }
                        processing = true
                        fetch(detailsUrl)
                            .then((res) => res.json())
                            .then((json) => {
                                if (json.seed_id) {
                                    seedId = json.seed_id
                                    clearInterval(poll)
                                    const seedUrl = `${url}/seed?key=${ff4apikey}&id=${seedId}`
                                    fetch(seedUrl)
                                        .then((res) => res.json())
                                        .then((seedDetails) => {
                                            updateRaceDetails(message, seedDetails, hasAdmin, hidden)
                                            sendMessage(message.channel, `The seed can be accessed at ${seedDetails.url}`)
                                        })
                                        .catch((err) => {
                                            tries += 1
                                            sendMessage(message.channel, `Error reading API response: ${err}`)
                                        })
                                } else if (json.error) {
                                    logger.logError(`Error: ${json}`)
                                    tries += 1
                                    sendMessage(message.channel, `Error generating seed: ${json.error}`)
                                    clearInterval(poll)
                                }
                                processing = false
                            })
                            .catch((err) => {
                                processing = false
                                sendMessage(message.channel, `Error talking to FE API: ${err}`)
                            })
                    }
                }, 1000)
            } else if (seedId) {
                const seedUrl = `${url}/seed?key=${ff4apikey}&id=${seedId}`
                fetch(seedUrl)
                    .then((res) => res.json())
                    .then((seedDetails) => {
                        updateRaceDetails(message, seedDetails, hasAdmin, hidden)
                        sendMessage(message, `The seed has been generated. Please use ${seedDetails.url} to generate the seed.`)
                    })
            }
        })
    return 'Seed generation pending...'
}
