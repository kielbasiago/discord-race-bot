const utils = require('../../race/race-utils')
const logger = require('../../../utils/logger')
const saveRace = require('../../../db/save-race')

const PRESETS = {
    race: { Flags: 'n,g,z,p,te', Title: 'Race Flags' },
    raceboss: { Flags: 'n,g,ro,z,p,te', Title: 'Race Flags + Boss Rando' },
    lostworlds: { Flags: 'n,g,l,z,p,te', Title: 'Lost Worlds' },
    lostworldsplus: { Flags: 'n,g,l,z,p,te', Title: 'Lost Worlds+' },
    legacyofcyrus: { Flags: 'n,loc,g,z,p,m,te', Title: 'Legacy of Cyrus' },
    crab: { Flags: 'n,g,z,p,tb,cr,te', Title: 'Crabsanity' },
    iceage: { Flags: 'n,ia,g,z,p,m,te', Title: 'Ice Age' },
    mystery: { Flags: '????', Title: 'Mystery' },
}

const updateRaceDetails = async (message, seedDetails) => {
    if (!message || !message.channel) { return }
    logger.info(seedDetails)
    const currentRace = await utils.getCurrentRace(message.channel.name)
    if (currentRace.metadata) {
        currentRace.metadata = Object.assign(currentRace.metadata, seedDetails)
    } else {
        currentRace.metadata = seedDetails
    }
    message.channel.setTopic(`${seedDetails.Title}`)
    saveRace(currentRace, { metadata: currentRace.metadata })
}

module.exports = async (message, args) => {
    const [preset] = args
    if (!preset) {
        return 'Please enter in a preset'
    }
    if (!Object.keys(PRESETS).includes(preset.toLowerCase())) {
        return `${preset} is not a valid preset. Please select from ${Object.keys(PRESETS).join(', ')}`
    }
    const seed = args.includes('--seed') ? args[args.indexOf('--seed') + 1] : ''

    await updateRaceDetails(message, Object.assign({}, ...[PRESETS[preset], seed ? { Seed: seed } : {}]))
    return `Race information updated with the ${preset} preset.`
}
