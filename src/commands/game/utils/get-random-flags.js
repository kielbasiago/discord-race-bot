const getRandomNumber = require('../../../utils/get-random-number')

const findMatchingUserFlag = (flagGroups, userFlags, group) => {
    const flagGroup = flagGroups[group]
    const allFlags = [...flagGroup.unique || [], flagGroup.optional || []]
    const [matchingUserFlag] = userFlags.filter((userFlag) => allFlags.some((f) => f.flag === userFlag.flag))
    return matchingUserFlag
}
const getRandomFlagOption = (selectedFlags, options, group) => {
    let newOptions = options
    if (!Array.isArray(options)) {
        newOptions = [options]
    }
    const updatedFlags = selectedFlags
    const numOptions = newOptions.length + 1 // Not including it is an option
    const option = getRandomNumber(0, numOptions)
    if (option < newOptions.length) {
        const flagValue = newOptions[option].flag
        if (typeof flagValue === 'object') {
            let subFlag = ''
            if (flagValue.unique) { subFlag = flagValue.unique[0].group } else { subFlag = flagValue.optional[0].group }
            if (subFlag !== group) {
                updatedFlags[group][subFlag] = true
            }
            processFlagOptions(updatedFlags, flagValue, subFlag) //eslint-disable-line
        } else {
            if (!updatedFlags[group]) {
                updatedFlags[group] = { }
            }
            updatedFlags[group][flagValue] = true
        }
    }
}

const getUniqueChoice = (selectedFlags, choices, group) => {
    getRandomFlagOption(selectedFlags, choices, group)
}
const getRandomOptions = (selectedFlags, options, group) => {
    const optionsToChoose = getRandomNumber(0, options.length)
    for (let chosen = 0; chosen < optionsToChoose; chosen += 1) {
        getRandomFlagOption(selectedFlags, options, group)
    }
}
const processFlagOptions = (selectedFlags, flagOptions, group) => {
    const updatedFlags = selectedFlags
    const uniqueChoices = flagOptions.unique
    const options = flagOptions.optional
    if (!selectedFlags[group]) {
        updatedFlags[group] = { }
    }
    if (uniqueChoices) {
        getUniqueChoice(updatedFlags, uniqueChoices, group)
    }
    if (options) {
        getRandomOptions(updatedFlags, options, group)
    }
}
const getRandomFlags = (selectedFlags, flagGroups, group, userFlags) => {
    const updatedFlags = selectedFlags
    const flagOptions = flagGroups[group]
    if (!updatedFlags[group]) {
        updatedFlags[group] = { }
    }
    const matchingUserFlag = findMatchingUserFlag(flagGroups, userFlags, group)
    if (matchingUserFlag) {
        updatedFlags[group][matchingUserFlag.flag] = true
        return
    }
    processFlagOptions(selectedFlags, flagOptions, group)
}
const setDefaultValues = (selectedFlags, flagGroups) => {
    const updatedFlags = selectedFlags
    Object.keys(flagGroups).forEach((group) => {
        Object.keys(flagGroups[group]).forEach((flag) => {
            updatedFlags[group] = { }
            flagGroups[group][flag].forEach((flg) => {
                updatedFlags[group][flg.flag] = false
            })
        })
    })
    return updatedFlags
}
module.exports = (flagGroups, userFlags) => {
    const selectedFlags = setDefaultValues({}, flagGroups)
    Object.keys(flagGroups).forEach((group) => {
        getRandomFlags(selectedFlags, flagGroups, group, userFlags)
    })
    return selectedFlags
}
