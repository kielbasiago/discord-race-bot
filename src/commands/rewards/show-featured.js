const moment = require('moment')
const getFeaturedUsers = require('../../db/user/get-featured-users')
const buildTable = require('../../utils/table-layout')

module.exports = async () => {
    const headers = ['Name', 'Cookies', 'Wins', 'Win %', 'Top 3 %', 'Stream', 'Badges', 'Featured Until']
    const featuredUsers = await getFeaturedUsers()
    featuredUsers.forEach((user) => {
        if (user.id) {
            delete user.id // eslint-disable-line 
        }
    })
    return `\`\`\`\n${buildTable(headers, featuredUsers.sort((a, b) => moment(b).subtract(moment(a))))}\n\`\`\``
}
