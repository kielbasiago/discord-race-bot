const createVoiceChannels = require('./create-voice-channels')

module.exports = {
    '!createvoicechannels': createVoiceChannels,
}
