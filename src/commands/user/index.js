const giveCookie = require('./give-cookie')
const getUserDetails = require('./get-details')
const setStreamInfo = require('./set-stream-info')
const cookieboard = require('./cookieboard')
const updateDisplayName = require('./update-user-displayname')
const resetPb = require('./reset-pb')
const eatCookie = require('./eat-cookie')

module.exports = {
    '!givecookie': giveCookie,
    '!userdetails': getUserDetails,
    '!setstream': setStreamInfo,
    '!cookiesheet': cookieboard,
    '!setdisplayname': updateDisplayName,
    '!resetpersonalbest': resetPb,
    '!eatcookie': eatCookie,
}
