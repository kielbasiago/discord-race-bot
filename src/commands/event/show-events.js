const sendDirectMessage = require('../../utils/send-direct-message')
const displayTable = require('../../utils/table-layout')
const getEvents = require('../../db/event/get-events')

module.exports = async (message) => {
    const events = await getEvents()
    const headers = ['Name', 'Type', 'Server', 'Entrants', 'Description']
    const rows = events.filter((ev) => ev.status !== 'Completed').map((ev) => ({
        name: ev.name,
        type: `${ev.entrantType} ${ev.type}`,
        server: ev.server || 'ALL',
        entrants: (ev.entrants && ev.entrants.length) || 0,
        description: (ev.description && ev.description.length > 50 ? `${ev.description.substring(0, 47)}...` : ev.description) || '',
    }))
    const returnValue = `\`\`\`\n${displayTable(headers, rows)}\n\`\`\``
    sendDirectMessage(message.author, returnValue)
    if (message.guild) {
        message.delete()
    }
    return null
}
