const createEvent = require('./create-event')
const showEvents = require('./show-events')
const eventSettings = require('./set-event-settings')
const endEvent = require('./end-event')
const getEventDetails = require('./get-event-details')
const addEventAdmin = require('./add-event-admin')
const joinEvent = require('./join-event')
const showEventAdmins = require('./show-event-admins')
const myEvents = require('./my-events')

module.exports = {
    '!createevent': createEvent,
    '!showevents': showEvents,
    '!eventsettings': eventSettings,
    '!finishevent': endEvent,
    '!eventdetails': getEventDetails,
    '!addeventadmin': addEventAdmin,
    '!joinevent': joinEvent,
    '!showeventadmins': showEventAdmins,
    '!myevents': myEvents,
}
