module.exports = [
    { name: 'Creator', level: 4 },
    { name: 'Lead', level: 3 },
    { name: 'Organizer', level: 2 },
    { name: 'Race', level: 1 },
]
