require('dotenv').config()

module.exports = (route, id) => {
    const baseUrl = process.env.DR_BOT_API_URL
    // TODO: Actual parsing
    let validatedRoute = decodeURIComponent(route).replace(/\s/g, '%20')
    if (id) {
        validatedRoute = validatedRoute.concat(`/${id}`)
    }
    return `${baseUrl}/${validatedRoute}`
}
