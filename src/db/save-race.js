const saveRowWithId = require('./util/save-row-with-id')
const raceCache = require('../cache/race-cache')

module.exports = async (race, updatedFields) => {
    await raceCache.clearCacheValue(race.id)
    await raceCache.clearCacheValue(race.key)
    await saveRowWithId('races', updatedFields, race.id)
}
