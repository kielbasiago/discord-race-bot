const createApiConnection = require('../util/create-api-connection')
const makeCallAndGetResponse = require('../util/make-call-and-get-response')

module.exports = async (userId) => {
    if (!userId) {
        return null
    }
    const uri = createApiConnection(`events?userId=${userId}`)
    const response = await makeCallAndGetResponse(uri, null, 'GET')
    return response
}
