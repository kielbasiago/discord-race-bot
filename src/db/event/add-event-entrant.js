const createApiConnection = require('../util/create-api-connection')
const makeCallAndGetResponse = require('../util/make-call-and-get-response')

module.exports = async (event, entrant) => {
    const uri = createApiConnection('events', `${event.id}/entrants`)
    const response = await makeCallAndGetResponse(uri, entrant, 'POST')
    return response
}
