const createApiConnection = require('../util/create-api-connection')
const makeCallAndGetResponse = require('../util/make-call-and-get-response')

module.exports = async (event, race) => {
    const uri = createApiConnection('events', `${event.id}/races`)
    const response = await makeCallAndGetResponse(uri, race, 'POST')
    return response
}
