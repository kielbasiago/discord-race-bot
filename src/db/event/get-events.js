const getTableData = require('../util/get-table-data')

module.exports = async () => getTableData('events')
