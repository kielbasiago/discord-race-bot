const saveRowWithId = require('../util/save-row-with-id')
const raceCache = require('../../cache/race-cache')

module.exports = async (race, finisher) => {
    await raceCache.clearCacheValue(race.id)
    await raceCache.clearCacheValue(race.key)
    await saveRowWithId('races', finisher, `${race.id}/finishers/${finisher.id}`)
}
