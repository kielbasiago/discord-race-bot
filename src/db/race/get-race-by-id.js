const getRowById = require('../util/get-row-by-id')
const raceCache = require('../../cache/race-cache')

module.exports = async (id) => {
    const cachedValue = await raceCache.getValue(id)
    if (!cachedValue) {
        const race = await getRowById('races', id)
        race.entrants = race.entrants.filter((en) => en) || []
        race.finishers = race.finishers.filter((en) => en) || []
        await raceCache.updateCacheValue(id, race)
        return race
    }
    return cachedValue
}
