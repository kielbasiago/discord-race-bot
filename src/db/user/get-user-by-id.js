const getRowById = require('../util/get-row-by-id')
const userCache = require('../../cache/user-cache')

module.exports = async (id) => {
    const cachedValue = await userCache.getValue(id)
    if (!cachedValue) {
        const userData = await getRowById('users', id)
        await userCache.updateCacheValue(id, userData)
        return userData
    }
    return cachedValue
}
