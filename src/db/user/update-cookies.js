const createApiConnection = require('../util/create-api-connection')
const makeCallAndGetResponse = require('../util/make-call-and-get-response')

module.exports = async (user, amount, source, reason) => {
    const uri = createApiConnection('users', `${user.id}/cookies`)
    const response = await makeCallAndGetResponse(uri, { source, cookieAmount: amount, reason }, 'PUT')
    return response
}
