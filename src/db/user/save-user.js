const saveRowWithId = require('../util/save-row-with-id')
const userCache = require('../../cache/user-cache')

module.exports = async (user, updatedFields) => {
    await userCache.clearCacheValue(user.id)
    await userCache.clearCacheValue(user.name.toLowerCase())
    await saveRowWithId('users', updatedFields, user.id)
}
