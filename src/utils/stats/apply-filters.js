const applyFilter = require('./apply-filter')

module.exports = (dataset, filters) => {
    let theDataset = dataset
    if (!filters || filters.length === 0) { return theDataset }
    filters.forEach((filter) => {
        theDataset = applyFilter(theDataset, filter)
    })
    return theDataset
}
