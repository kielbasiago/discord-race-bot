const moment = require('moment')

module.exports = (dataset, filter) => {
    if (!dataset || dataset.length === 0) return dataset
    let theDataset = dataset
    switch (filter.name) {
    case '--game':
        theDataset = theDataset.filter((race) => race.type.some((t) => t.includes(filter.value)))
        break
    case '--start':
        theDataset = theDataset.filter((race) => moment(race.startTime).diff(moment(filter.value)) >= 0)
        break
    case '--end':
        theDataset = theDataset.filter((race) => moment(race.finishTime).diff(moment(filter.value)) <= 0)
        break
    case '--numentrants':
        theDataset = theDataset.filter((race) => race.entrants.length >= Number(filter.value))
        break
    case '--ff4flags':
        theDataset = theDataset.filter((row) => row.metadata && row.metadata.Flags === filter.value)
        break
    case '--entrant':
        theDataset = theDataset.filter((row) => row.entrants.some((entrant) => entrant.name && entrant.name.toLowerCase() === filter.value.toLowerCase()))
        break
    case '--team':
        theDataset = theDataset.filter((row) => row.entrants.some((entrant) => entrant.team && entrant.team.toLowerCase() === filter.value.toLowerCase()))
        break
    case '--roller':
        theDataset = theDataset.filter((row) => row.metadata && row.metadata.Roller && row.metadata.Roller.toLowerCase() === filter.value.toLowerCase())
        break
    default:
        break
    }
    return theDataset
}
