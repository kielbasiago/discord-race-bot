const raceUtils = require('../commands/race/race-utils')

module.exports = async (message, options) => {
    if (!message) {
        return false
    }
    if (options) {
        if (options.checkGuild) {
            if (!message.guild) { return false }
        }
        if (options.channelType) {
            if (message.channel.type !== options.channelType) { return false }
        }
        if (options.channelName) {
            if (message.channel.name !== options.channelName) { return false }
        }
        if (options.checkArgs) {
            const args = message.content.split(' ')
            if (args.length < 2) { return false }
        }
        if (options.checkCommand) {
            if (!(message.content.startsWith('!') || message.content.startsWith('.'))) {
                return false
            }
        }
        if (options.checkRace) {
            const currentRace = await raceUtils.getCurrentRace(message.channel.name)
            if (!currentRace) {
                return false
            }
        }
    }
    return true
}
