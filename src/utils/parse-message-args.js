/* eslint-disable */
module.exports = (message) => {
  if (!message.content) { return null }
  // Magical function stolen from the below
  // https://stackoverflow.com/questions/2817646/javascript-split-string-on-space-or-on-quotes-to-array
  // To heck with smart quotes

  let parsed = replaceSmartQuotes(message.content).match(/\\?.|^$/g).reduce((p, c) => {
    if (c === '"') {
      p.quote ^= 1
    } else if (!p.quote && c === ' ') {
      p.a.push('')
    } else {
      p.a[p.a.length - 1] += c.replace(/\\(.)/, '$1')
    }
    return p
  }, { a: [''] }).a
  parsed = parsed.filter(arg => arg !== '')
  return parsed
}

const GODDAMN_SMART_QUOTES = [
  '\u0060',
  '\u00B4',
  '\u2018',
  '\u2019',
  '\u201C',
  '\u201D',
]
const replaceSmartQuotes = (content) => {
  return GODDAMN_SMART_QUOTES.reduce((replaced, sq) => replaced.replace(sq, '"'), content)
}
