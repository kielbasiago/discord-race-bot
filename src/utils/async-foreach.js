module.exports = async (array, callback) => {
    const promises = []
    for (let index = 0; index < array.length; index += 1) {
        promises.push(callback(array[index], index, array))
    }
    await Promise.all(promises)
}
