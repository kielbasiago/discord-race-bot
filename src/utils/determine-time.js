const moment = require('moment')
const determineTimeByDuration = require('./determine-time-by-duration')

module.exports = (start, end) => {
    const duration = moment.duration(moment(end).diff(moment(start)))
    return determineTimeByDuration(duration)
}
