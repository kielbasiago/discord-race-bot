const isMessageOnRaceServer = (message, race) => {
    if (!message || !race) { return false }
    if (!message.guild || !race.guild) { return false }
    if (!message.guild.id || !race.guild.id) { return false }
    return message.guild.id === race.guild.id
}
const isSentByCreatorOrAdmin = (message, race, hasAdmin) => {
    if (hasAdmin) { return true }
    if (!message || !race) {
        return false
    }
    return message.author.id === race.creator.id
}

const verifyRaceMessage = (message, race) => {
    if (!isMessageOnRaceServer(message, race)) {
        return `This message must be sent on ${race.guild.name}`
    }
    return null
}

module.exports = {
    isMessageOnRaceServer,
    isSentByCreatorOrAdmin,
    verifyRaceMessage,
}
