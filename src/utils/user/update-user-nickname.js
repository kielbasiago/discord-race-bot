module.exports = (guildDetails, nickname) => {
    let updatedGuildDetails = guildDetails
    if (!updatedGuildDetails) {
        updatedGuildDetails = { nickname, display_name: nickname }
    }
    if (updatedGuildDetails.nickname === updatedGuildDetails.display_name) {
        updatedGuildDetails.display_name = nickname
    }
    updatedGuildDetails.nickname = nickname
    return updatedGuildDetails
}
