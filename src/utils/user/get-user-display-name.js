module.exports = (user, guild) => {
    if (!guild) {
        return user.name
    }
    if (!user.guildDetails) {
        return user.name
    }
    const guildDetails = user.guildDetails[guild.id]
    if (!guildDetails) {
        return user.name
    }
    if (!guildDetails.display_name) {
        return user.name
    }
    return guildDetails.display_name
}
