const randomString = require('randomstring')

module.exports = (length = 8) => randomString.generate({ length, charset: 'alphanumeric' })
