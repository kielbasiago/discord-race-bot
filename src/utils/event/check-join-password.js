const checkPassword = require('./check-password')

module.exports = (event, password) => {
    if (!event || !event.joinPassword || !password) { return false }
    return checkPassword(event.joinPassword, password)
}
