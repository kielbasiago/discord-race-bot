const significantMetadata = [
    'Hash',
    'Url',
    'Flags',
    'Title',
    'Weights',
    'Weapon',
    'Aspect',
    'Heat',
    'Blame',
    'Roller',
]

module.exports = (race) => {
    if (!race) {
        return 'No race was found, have a developer check for errors.'
    }
    let raceMessage = `${race.key} (${race.mode}) - Status: ${race.status} | ${race.entrants.length} Entrants`
    if (race.metadata) {
        significantMetadata.forEach((key) => {
            if (race.metadata[key]) {
                raceMessage += ` | ${key} = ${race.metadata[key]}`
            }
        })
    }
    if (race.event) {
        raceMessage = `Event: ${race.event.name} - ${raceMessage}`
    }
    return raceMessage
}
