const getRaceStatusMessage = require('./get-race-status-message')
const getServerConfigValue = require('../get-config-value')
const asyncForEach = require('../async-foreach')
const CONSTANTS = require('../../constants')

module.exports = async (client, race) => {
    if (!client) { return }
    if (!race) { return }
    if (!race.messages || race.messages.length < 1) { return }
    const raceStatusMessageBase = getRaceStatusMessage(race)
    await asyncForEach(race.messages, async (alertDetails) => {
        const guild = client.guilds.cache.find((gld) => gld.id === alertDetails.guildId)
        // Hacky AF
        const raceAlertRole = await getServerConfigValue({ guild }, CONSTANTS.SERVER_CONFIG.RACE_ALERT_ROLE)
        const channel = guild.channels.cache.find((chn) => chn.id === alertDetails.channelId)
        channel.messages.fetch(alertDetails.messageId)
            .then((msg) => {
                if (!msg) { return }
                let raceStatusMessage = raceStatusMessageBase
                if (raceAlertRole) {
                    const role = msg.guild.roles.cache.find((rl) => rl.name === raceAlertRole)
                    if (role) {
                        raceStatusMessage = `${role} - ${raceStatusMessage}`
                    }
                }
                if (msg.content !== raceStatusMessage) {
                    msg.edit(raceStatusMessage)
                }
            })
    })
}
