const test = require('tape')
const proxyquire = require('proxyquire')
const sinon = require('sinon')

const utilsStub = { }
const securityStub = sinon.fake()
securityStub.verifyRaceMessage = () => null
let saveStub = sinon.fake()
let assignTeams = proxyquire('../../../src/commands/race/assign-teams', {
    './race-utils': utilsStub,
    '../../db/race/save-entrant': saveStub,
    '../../utils/security/security-utils': securityStub,
})

test('It should return early if no args or a number is sent as the first arg', async (t) => {
    t.equals(await assignTeams(null, null), 'The # of players per team must be sent in.', 'An error is returned if no args are sent in')
    t.equals(await assignTeams(null, []), 'The # of players per team must be sent in.', 'An error message is returned if no args are sent in')
    t.equals(await assignTeams(null, ['test']), 'The # of players per team must be sent in.', 'An error message is returned if the first argument is not numeric.')
    t.end()
})
test('It should return early if there is no race in the current channel', async (t) => {
    utilsStub.getCurrentRace = () => null
    t.equals(await assignTeams({ channel: { name: 'Test' } }, [2]), 'No race is currently active in this channel.', 'An error message is returned if no race is active.')
    t.end()
})
test('It should return early if the race status is not open', async (t) => {
    utilsStub.getCurrentRace = name => ({ key: name, status: 'RUNNING' })
    t.equals(await assignTeams({ channel: { name: 'Test' } }, [2]), 'Cannot set teams while the race is ongoing.', 'An error message is returned if the current race is not open.')
    t.end()
})
test('It should return early if the number entered is negative', async (t) => {
    t.equals(await assignTeams(null, [-3]), 'The # of players per team must be a positive number.', 'An error message is returned if the number of players per team is negative.')
    t.equals(await assignTeams(null, [0]), 'The # of players per team must be a positive number.', 'An error message is returned if the number of players per team is zero.')
    t.end()
})
test('It should return early if the number of players per team is greater than the number of entrants', async (t) => {
    utilsStub.getCurrentRace = name => ({ key: name, status: 'Open', entrants: [{ }, { }] })
    t.equals(await assignTeams({ channel: { name: 'Test' } }, [3]),
        'There are not enough players in the race to make a team of 3',
        'An error is returned if the players per team is greater than the number of entrants.')
    t.end()
})
test('It should send a message if even teams cannot be done', async (t) => {
    utilsStub.getCurrentRace = name => ({ key: name, status: 'Open', entrants: [{ name: 'Test1' }, { name: 'Test2' }, { name: 'Test3' }] })
    const channel = { name: 'Test', send: sinon.fake() }
    const message = await assignTeams({ channel }, [2])
    t.notEquals(message, null, 'A message with teams was returned.')
    t.equals(channel.send.callCount, 1, 'Send was called on the channel.')
    t.equals(channel.send.getCall(0).lastArg,
        'Unfortunately, there are not enough players to make an even number of teams.  Some players will be left unassigned.',
        'A message was sent to the channel warning about uneven teams.')
    t.end()
})
test('It should automatically assign teams if the autoassign flag is passed in', async (t) => {
    saveStub = sinon.fake()
    assignTeams = proxyquire('../../../src/commands/race/assign-teams', {
        './race-utils': utilsStub,
        '../../db/race/save-entrant': saveStub,
        '../../utils/security/security-utils': securityStub,
    })
    const race = {
        key: 'Test',
        status: 'Open',
        entrants: [
            { name: 'Test1' },
            { name: 'Test2', team: 'Test2' },
            { name: 'Test3', team: 'Test3' },
            { name: 'Test4' },
            { name: 'Test5', team: 'The Testers' },
            { name: 'Test6', team: 'The Testers' },
        ],
    }
    utilsStub.getCurrentRace = async () => race
    const channel = { name: 'Test', send: sinon.fake() }
    const message = await assignTeams({ channel }, [2, '--everyone'])
    t.notEquals(message, null, 'A message with teams was returned.')
    t.equals(channel.send.callCount, 0, 'Send was not called on the channel.')
    t.notEquals(race.entrants[0].team, null, "The first entrant's team was set.")
    t.notEquals(race.entrants[1].team, null, "The second entrant's team was set.")
    t.notEquals(race.entrants[2].team, null, "The third entrant's team was set.")
    t.notEquals(race.entrants[3].team, null, "The fourth entrant's team was set.")
    t.notEquals(race.entrants[4].team, 'The Testers', "The fifth entrant's team was overridden.")
    t.notEquals(race.entrants[5].team, 'The Testers', "The sixth entrant's team was overridden.")
    t.equals(saveStub.callCount, 6, 'Save was called after specifying everyone.')
    t.end()
})
test('It should automatically assign teams unless the --dryrun flag is passed in, and should not overwrite if the --everyone flag is sent in', async (t) => {
    saveStub = sinon.fake()
    assignTeams = proxyquire('../../../src/commands/race/assign-teams', {
        './race-utils': utilsStub,
        '../../db/race/save-entrant': saveStub,
        '../../utils/security/security-utils': securityStub,
    })
    const race = {
        key: 'Test',
        status: 'Open',
        entrants: [
            { name: 'Test1' },
            { name: 'Test2', team: 'Test2' },
            { name: 'Test3', team: 'Test3' },
            { name: 'Test4' },
            { name: 'Test5', team: 'The Testers' },
            { name: 'Test6', team: 'The Testers' },
        ],
    }
    utilsStub.getCurrentRace = async () => race
    const channel = { name: 'Test', send: sinon.fake() }
    const message = await assignTeams({ channel }, [2])
    t.notEquals(message, null, 'A message with teams was returned.')
    t.equals(channel.send.callCount, 0, 'Send was not called on the channel.')
    t.notEquals(race.entrants[0].team, null, "The first entrant's team was set.")
    t.notEquals(race.entrants[1].team, null, "The second entrant's team was set.")
    t.notEquals(race.entrants[2].team, null, "The third entrant's team was set.")
    t.notEquals(race.entrants[3].team, null, "The fourth entrant's team was set.")
    t.equals(race.entrants[4].team, 'The Testers', "The fifth entrant's team was not overridden.")
    t.equals(race.entrants[5].team, 'The Testers', "The sixth entrant's team was not overridden.")
    t.equals(saveStub.callCount, 4, 'Save was called after auto assigning.')
    t.end()
})
