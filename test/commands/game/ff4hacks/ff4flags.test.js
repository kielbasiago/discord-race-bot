const test = require('tape')
const proxyquire = require('proxyquire')
const sinon = require('sinon')

const sendMessageStub = sinon.fake()
const raceUtilsStub = {
    getCurrentRace: sinon.fake(),
}
const randomStub = {
    generate: () => 'TESTSEED',
}
const saveStub = sinon.fake()
const fetchFake = sinon.fake()
const fetchStub = async (url) => {
    fetchFake(url)
    return {
        json: () => ({
            status: 'exists',
            seed_id: 'test',
        }),
    }
}
process.env.FE_API_KEY = 'test'

const ff4flags = proxyquire('../../../../src/commands/game/ff4hacks/ff4flags',
    {
        'node-fetch': fetchStub,
        '../../../utils/send-message': sendMessageStub,
        '../../race/race-utils': raceUtilsStub,
        '../../../db/save-race': saveStub,
        randomstring: randomStub,
    })

test('FF4Flags returns error messages if no flags are given', async (t) => {
    t.is(await ff4flags({ }, null), 'No flags were given.', 'An error message is returned if no flags are input.')
    t.is(await ff4flags({ }, []), 'No flags were given.', 'An error message is returned if no flags are input.')
    t.is(saveStub.callCount, 0, 'Nothing was called to save.')
    t.is(fetchFake.callCount, 0, 'Fetch was not called when no flags were given.')
    t.end()
})
