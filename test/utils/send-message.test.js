const test = require('tape')
const sinon = require('sinon')
const sendMessage = require('../../src/utils/send-message')

test('A message should be sent to the fake channel', async (t) => {
    const channel = { send: sinon.fake() }
    await sendMessage(channel, 'A test message')
    t.equals(1, channel.send.callCount, 'The send method should be called once')
    t.equals(true, channel.send.getCall(0).lastArg === 'A test message', 'The message should be passed in fully')
    t.end()
})
test('A really long message should be broken up and sent as separate messages to a guild channel', async (t) => {
    const channel = { send: sinon.fake(), type: 'GUILD_TEXT' }
    const testMessage = 'A'.repeat(2000).concat('B'.repeat(2000)).concat('C'.repeat(2000)).concat('D'.repeat(2000))
    await sendMessage(channel, testMessage)
    t.equals(5, channel.send.callCount, '5 messages were sent after the message was broken up.')
    t.equals(channel.send.getCall(0).lastArg, `\`\`\`\n${'A'.repeat(1992)}\n\`\`\``, 'The first message was correct.')
    t.equals(channel.send.getCall(1).lastArg, `\`\`\`\n${'A'.repeat(8)}${'B'.repeat(1984)}\n\`\`\``, 'The second message was correct.')
    t.equals(channel.send.getCall(2).lastArg, `\`\`\`\n${'B'.repeat(16)}${'C'.repeat(1976)}\n\`\`\``, 'The third message was correct.')
    t.equals(channel.send.getCall(3).lastArg, `\`\`\`\n${'C'.repeat(24)}${'D'.repeat(1968)}\n\`\`\``, 'The fourth message was correct.')
    t.equals(channel.send.getCall(4).lastArg, `\`\`\`\n${'D'.repeat(32)}\n\`\`\``, 'The fifth message was correct.')
    t.end()
})
test('A really long message should be broken up and sent as separate messages to a DM channel', async (t) => {
    const channel = { send: sinon.fake(), type: 'DM' }
    const testMessage = 'A'.repeat(2000).concat('B'.repeat(2000)).concat('C'.repeat(2000)).concat('D'.repeat(2000))
    await sendMessage(channel, testMessage)
    t.equals(5, channel.send.callCount, '5 messages were sent after the message was broken up.')
    t.equals(channel.send.getCall(0).lastArg, `\`\`\`\n${'A'.repeat(1992)}\n\`\`\``, 'The first message was correct.')
    t.equals(channel.send.getCall(1).lastArg, `\`\`\`\n${'A'.repeat(8)}${'B'.repeat(1984)}\n\`\`\``, 'The second message was correct.')
    t.equals(channel.send.getCall(2).lastArg, `\`\`\`\n${'B'.repeat(16)}${'C'.repeat(1976)}\n\`\`\``, 'The third message was correct.')
    t.equals(channel.send.getCall(3).lastArg, `\`\`\`\n${'C'.repeat(24)}${'D'.repeat(1968)}\n\`\`\``, 'The fourth message was correct.')
    t.equals(channel.send.getCall(4).lastArg, `\`\`\`\n${'D'.repeat(32)}\n\`\`\``, 'The fifth message was correct.')
    t.end()
})
test('A call to the function without a channel does not do anything', async (t) => {
    t.equals(null, await sendMessage(null, 'Some message'), 'The function returned without doing anything')
    t.end()
})
test('A call to the function without a message returns early', async (t) => {
    const channel = { send: sinon.fake(), type: 'GUILD_TEXT' }
    t.equals(null, await sendMessage(channel, null), 'The function returns early if no message is entered')
    t.end()
})
test('A message with a table should split on a row', async (t) => {
    const channel = { send: sinon.fake(), type: 'DM' }
    const testMessage = 'A'.repeat(1990).concat('\n').concat('B'.repeat(1990).concat('\n')).concat('C'.repeat(1990).concat('\n'))
        .concat('D'.repeat(1990))
    await sendMessage(channel, testMessage)
    t.equals(4, channel.send.callCount, '4 messages were sent after the message was broken up.')
    t.equals(channel.send.getCall(0).lastArg, `\`\`\`\n${'A'.repeat(1990)}\n\`\`\``, 'The first message was correct.')
    t.equals(channel.send.getCall(1).lastArg, `\`\`\`\n\n${'B'.repeat(1990)}\n\`\`\``, 'The second message was correct.')
    t.equals(channel.send.getCall(2).lastArg, `\`\`\`\n\n${'C'.repeat(1990)}\n\`\`\``, 'The third message was correct.')
    t.equals(channel.send.getCall(3).lastArg, `\`\`\`\n\n${'D'.repeat(1990)}\n\`\`\``, 'The fourth message was correct.')
    t.end()
})
