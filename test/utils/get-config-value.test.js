const test = require('tape')
const proxyquire = require('proxyquire')
const sinon = require('sinon')

const configData = { }
const configStub = async () => configData
const saveStub = sinon.fake()
const getConfigValue = proxyquire('../../src/utils/get-config-value', {
    '../db/get-server-config-by-id': configStub,
    './set-config-value': saveStub,
})

test('The default value is returned if no guild exists on the message', async (t) => {
    const message = { }
    t.equals(await getConfigValue(message, 'key', 'Default Value'), 'Default Value', 'The default value is returned if there is no guild.')
    t.end()
})
test('The default value is returned if no configs were found', async (t) => {
    const message = { guild: 'test' }
    t.equals(await getConfigValue(message, 'key', 'Default Value'), 'Default Value', 'The default value is returned if there was no config found.')
    t.end()
})
test('The default value is returned if the key is not found', async (t) => {
    const message = { guild: 'test' }
    configData.id = '123'
    configData.key = 'test'
    t.equals(await getConfigValue(message, 'notakey', 'Default Value'), 'Default Value', 'The default value is returned if the key is not present.')
    t.end()
})
test('The config key is returned if it is found', async (t) => {
    const message = { guild: { id: 'testguildid' } }
    configData.id = 'testguildid'
    configData.testkey = 'value'
    t.equals(await getConfigValue(message, 'testkey', 'Default Value'), 'value', 'The key value was returned.')
    t.end()
})
test('The default value is returned if no configs are found', async (t) => {
    Object.keys(configData).forEach(key => delete configData[key])
    const message = { guild: { id: 'testguildid' } }
    t.equals(await getConfigValue(message, 'testkey', 'Default Value'), 'Default Value', 'The key value was returned.')
    t.end()
})
