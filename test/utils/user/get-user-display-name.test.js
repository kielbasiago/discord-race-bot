const test = require('tape')
const getUserDisplayName = require('../../../src/utils/user/get-user-display-name')

test('It should return the user name if there is no guild', (t) => {
    t.is(getUserDisplayName({ name: 'test' }), 'test', 'The user name is returned if no guild is input.')
    t.is(getUserDisplayName({ name: 'test' }, { }), 'test', 'The user name is returned if there are no guild details.')
    t.is(getUserDisplayName({ name: 'test', guildDetails: { test1: { } } }), 'test', 'The user name is returned if no matching guild details are found.')
    t.end()
})

test('It should return the user name if no display name is entered for the matching guild', (t) => {
    t.is(getUserDisplayName({ name: 'test', guildDetails: { test1: { } } }, { id: 'test1' }), 'test', 'The user name is returned if no matching guild display name is found.')
    t.end()
})

test('It should return the display name when found', (t) => {
    t.is(getUserDisplayName({
        name: 'test',
        guildDetails: { test1: { display_name: 'Test2' } },
    }, { id: 'test1' }), 'Test2', 'The user name is returned if no matching guild display name is found.')
    t.end()
})
