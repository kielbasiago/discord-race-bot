const test = require('tape')
const updateUserNickname = require('../../../src/utils/user/update-user-nickname')

test('The nickname and display name are updated as expected', (t) => {
    let details = null
    details = updateUserNickname(details, 'Test')
    t.same(details, { nickname: 'Test', display_name: 'Test' }, 'The details were created if they did not exist.')
    details = updateUserNickname(details, 'Test2')
    t.same(details, { nickname: 'Test2', display_name: 'Test2' }, 'The details were updated if they did exist.')

    details.display_name = 'Test_0'
    details = updateUserNickname(details, 'Test3')
    t.same(details, { nickname: 'Test3', display_name: 'Test_0' }, 'The details were updated and the display name was not if the display name did not match the nickname.')

    t.end()
})
