const test = require('tape')

const calculateWagerAmounts = require('../../../src/utils/race/calculate-wager-amounts')

test('It should correctly calculate the right earnings for cookie wagers', (t) => {
    const race = {
        totalWagered: 2700,
        finishers: [
            { placement: 1, wager: 100 },
            { placement: 2, wager: 200 },
            { placement: 3, wager: 300 },
            { placement: 4, wager: 500 },
            { placement: 5, wager: 600 },
            { placement: 6, wager: 1000 },
        ],
    }
    race.entrants = race.finishers
    calculateWagerAmounts(race)
    t.equals(race.finishers[0].amountWon, 603, 'First place should have won 603 cookies')
    t.equals(race.finishers[1].amountWon, 1202, 'Second place should have won 1202 cookies')
    t.equals(race.finishers[2].amountWon, 678, 'Third place should have won 678 cookies')
    t.equals(race.finishers[3].amountWon, 226, 'Fourth place should have won 226 cookies')
    t.equals(race.finishers[4].amountWon, 1, 'Fifth place should have won 1 cookies')
    t.equals(race.finishers[5].amountWon, 1, 'Sixth place should have won 1 cookies')
    t.end()
})

test('It should correctly calculate the right earnings for cookie wagers when nobody wagers', (t) => {
    const race = {
        totalWagered: 0,
        finishers: [
            { placement: 1 },
            { placement: 2 },
            { placement: 3 },
            { placement: 4 },
            { placement: 5 },
            { placement: 6 },
        ],
    }
    race.entrants = race.finishers
    calculateWagerAmounts(race)
    t.equals(race.finishers[0].amountWon, 3, 'First place should have won 3 cookies')
    t.equals(race.finishers[1].amountWon, 2, 'Second place should have won 2 cookies')
    t.equals(race.finishers[2].amountWon, 1, 'Third place should have won 1 cookies')
    t.equals(race.finishers[3].amountWon, 1, 'Fourth place should have won 1 cookies')
    t.equals(race.finishers[4].amountWon, 1, 'Fifth place should have won 1 cookies')
    t.equals(race.finishers[5].amountWon, 1, 'Sixth place should have won 1 cookies')
    t.end()
})

test('It should correctly calculate the right earnings for cookie wagers when there is a mixture of wagers', (t) => {
    const race = {
        totalWagered: 610,
        finishers: [
            { placement: 1 },
            { placement: 2, wager: 400 },
            { placement: 3, wager: 100 },
            { placement: 4 },
            { placement: 5, wager: 10 },
            { placement: 6, wager: 100 },
        ],
    }
    race.entrants = race.finishers
    calculateWagerAmounts(race)
    t.equals(race.finishers[0].amountWon, 3, 'First place should have won 3 cookies')
    t.equals(race.finishers[1].amountWon, 462, 'Second place should have won 462 cookies')
    t.equals(race.finishers[2].amountWon, 115, 'Third place should have won 115 cookies')
    t.equals(race.finishers[3].amountWon, 1, 'Fourth place should have won 1 cookies')
    t.equals(race.finishers[4].amountWon, 11, 'Fifth place should have won 11 cookies')
    t.equals(race.finishers[5].amountWon, 27, 'Sixth place should have won 27 cookies')
    t.end()
})

test('It should correctly calculate the right earnings for cookie wagers when there is a mixture of wagers', (t) => {
    const race = {
        totalWagered: 6000,
        finishers: [
            { placement: 1, wager: 1000 },
            { placement: 2, wager: 1000 },
            { placement: 3, wager: 1000 },
            { placement: 4, wager: 1000 },
            { placement: 5, wager: 1000 },
            { placement: 6, wager: 1000 },
        ],
    }
    race.entrants = race.finishers
    calculateWagerAmounts(race)
    t.equals(race.finishers[0].amountWon, 4506, 'First place should have won 4506 cookies')
    t.equals(race.finishers[1].amountWon, 1127, 'Second place should have won 1127 cookies')
    t.equals(race.finishers[2].amountWon, 376, 'Third place should have won 376 cookies')
    t.equals(race.finishers[3].amountWon, 1, 'Fourth place should have won 1 cookies')
    t.equals(race.finishers[4].amountWon, 1, 'Fifth place should have won 1 cookies')
    t.equals(race.finishers[5].amountWon, 1, 'Sixth place should have won 1 cookies')
    t.end()
})

test('It should correctly calculate the right earnings for cookie wagers when there is a mixture of wagers', (t) => {
    const race = {
        totalWagered: 69,
        finishers: [
            { placement: 1, wager: 12 },
            { placement: 2, wager: 12 },
            { placement: 3, wager: 11 },
            { placement: 4, wager: 0 },
            { placement: 5, wager: 10 },
            { placement: 6, wager: 1 },
            { placement: 7, wager: 2 },
            { placement: 8, wager: 10 },
            { placement: 9, wager: 10 },
            { placement: 10, wager: 0 },
            { placement: 11, wager: 1 },
        ],
    }
    race.entrants = race.finishers
    calculateWagerAmounts(race)
    t.equals(race.finishers[0].amountWon, 62, 'First place should have won 62 cookies')
    t.equals(race.finishers[1].amountWon, 15, 'Second place should have won 15 cookies')
    t.equals(race.finishers[2].amountWon, 6, 'Third place should have won 6 cookies')
    t.equals(race.finishers[3].amountWon, 1, 'Fourth place should have won 1 cookies')
    t.equals(race.finishers[4].amountWon, 1, 'Fifth place should have won 1 cookies')
    t.equals(race.finishers[5].amountWon, 1, 'Sixth place should have won 1 cookies')
    t.equals(race.finishers[6].amountWon, 1, 'Seventh place should have won 1 cookies')
    t.equals(race.finishers[7].amountWon, 1, 'Eighth place should have won 1 cookies')
    t.equals(race.finishers[8].amountWon, 1, 'Ninth place should have won 1 cookies')
    t.equals(race.finishers[9].amountWon, 1, 'Tenth place should have won 1 cookies')
    t.equals(race.finishers[10].amountWon, 1, 'Eleventh place should have won 1 cookies')
    t.end()
})

test('It should correctly calculate the right earnings for cookie wagers when there is a mixture of wagers', (t) => {
    const race = {
        totalWagered: 200,
        finishers: [
            { placement: 1, wager: 28 },
            { placement: 2, wager: 30 },
            { placement: 3, wager: 0 },
            { placement: 4, wager: 0 },
            { placement: 5, wager: 101 },
            { placement: 6, wager: 3 },
            { placement: 7, wager: 1 },
            { placement: 8, wager: 37 },
        ],
    }
    race.entrants = race.finishers
    delete race.finishers[7]
    calculateWagerAmounts(race)
    t.equals(race.finishers[0].amountWon, 143, 'First place should have won 143 cookies')
    t.equals(race.finishers[1].amountWon, 51, 'Second place should have won 51 cookies')
    t.equals(race.finishers[2].amountWon, 1, 'Third place should have won 1 cookies')
    t.equals(race.finishers[3].amountWon, 1, 'Fourth place should have won 1 cookies')
    t.equals(race.finishers[4].amountWon, 15, 'Fifth place should have won 15 cookies')
    t.equals(race.finishers[5].amountWon, 1, 'Sixth place should have won 1 cookies')
    t.equals(race.finishers[6].amountWon, 1, 'Seventh place should have won 1 cookies')
    t.end()
})

test('It should correctly calculate the right earnings for cookie wagers when there is a mixture of wagers', (t) => {
    const race = {
        totalWagered: 30,
        finishers: [
            { placement: 1, wager: 10 },
            { placement: 2, wager: 10 },
            { placement: 3, wager: 10 },
        ],
    }
    race.entrants = race.finishers
    calculateWagerAmounts(race)
    t.equals(race.finishers[0].amountWon, 27, 'First place should have won 27 cookies')
    t.equals(race.finishers[1].amountWon, 9, 'Second place should have won 9 cookies')
    t.equals(race.finishers[2].amountWon, 1, 'Third place should have won 1 cookies')
    t.end()
})

test('It should correctly calculate the right earnings for cookies wagers when donations are involved', (t) => {
    const race = {
        totalWagered: 69,
        finishers: [
            { placement: 1, wager: 12 },
            { placement: 2, wager: 12 },
            { placement: 3, wager: 11 },
            { placement: 4, wager: 0 },
            { placement: 5, wager: 10 },
            { placement: 6, wager: 1 },
            { placement: 7, wager: 2 },
            { placement: 8, wager: 10 },
            { placement: 9, wager: 10 },
            { placement: 10, wager: 0 },
            { placement: 11, wager: 1 },
        ],
        donations: [
            { name: 'test', amount: 500 },
            { name: 'test2', amount: 123 },
            { name: 'test3', amount: 1 },
        ],
    }
    race.entrants = race.finishers
    calculateWagerAmounts(race)
    t.equals(race.finishers[0].amountWon, 374, 'First place should have won 374 cookies')
    t.equals(race.finishers[1].amountWon, 233, 'Second place should have won 233 cookies')
    t.equals(race.finishers[2].amountWon, 99, 'Third place should have won 99 cookies')
    t.equals(race.finishers[3].amountWon, 1, 'Fourth place should have won 1 cookies')
    t.equals(race.finishers[4].amountWon, 1, 'Fifth place should have won 1 cookies')
    t.equals(race.finishers[5].amountWon, 1, 'Sixth place should have won 1 cookies')
    t.equals(race.finishers[6].amountWon, 1, 'Seventh place should have won 1 cookies')
    t.equals(race.finishers[7].amountWon, 1, 'Eighth place should have won 1 cookies')
    t.equals(race.finishers[8].amountWon, 1, 'Ninth place should have won 1 cookies')
    t.equals(race.finishers[9].amountWon, 1, 'Tenth place should have won 1 cookies')
    t.equals(race.finishers[10].amountWon, 1, 'Eleventh place should have won 1 cookies')
    t.end()
})
