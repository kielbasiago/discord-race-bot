const test = require('tape')
const buildTable = require('../../src/utils/table-layout')

test('It should build a table', (t) => {
    const headers = ['This', 'Is', 'My Very Awesome and Totally Functional', 'Test']
    const data = [{
        col1: 'Small',
        col2: 'A',
        col3: 'Not as long',
        col4: 'D',
    },
    {
        col1: 'A',
        col2: 'B',
        col3: 'C',
        col4: 'Hello',
    }]

    const theTable = buildTable(headers, data)
    const expected = 'This  Is My Very Awesome and Totally Functional Test  \n'
    + '------------------------------------------------------\n'
    + 'Small A  Not as long                            D     \n'
    + 'A     B  C                                      Hello \n'
    t.equals(theTable, expected, 'The table was built correctly')
    t.end()
})
test('It should return if no headers or data are entered', (t) => {
    t.is(buildTable(null, null), null, 'Undefined is returned if no args are valid.')
    t.is(buildTable(null, [{ test: 'A' }]), null, 'Undefined is returned if no headers are passed in')
    t.is(buildTable(['test', 'something', 'else'], null), 'test something else', 'Undefined is returned if no data is passed in.')

    t.end()
})
test('It should use the lesser of length of columns and headers are not equal', (t) => {
    const headers = ['This', 'My Very Awesome and Totally Functional', 'Test']
    const data = [{
        col1: 'Small',
        col2: 'A',
        col3: 'Not as long',
        col4: 'D',
    },
    {
        col1: 'A',
        col2: 'B',
        col3: 'C',
        col4: 'Hello',
    }]

    const theTable = buildTable(headers, data)
    const expected = 'This  My Very Awesome and Totally Functional Test        \n'
    + '---------------------------------------------------------\n'
    + 'Small A                                      Not as long \n'
    + 'A     B                                      C           \n'
    t.equals(theTable, expected, 'The table was built correctly')
    t.end()
})
test('It should use the lesser of length of columns and headers are not equal', (t) => {
    const headers = ['This', 'Is', 'My Very Awesome and Totally Functional', 'Test']
    const data = [{
        col1: 'Small',
        col2: 'A',
        col3: 'Not as long',
    },
    {
        col1: 'A',
        col2: 'B',
        col3: 'C',
    }]

    const theTable = buildTable(headers, data)
    const expected = 'This  Is My Very Awesome and Totally Functional \n'
    + '------------------------------------------------\n'
    + 'Small A  Not as long                            \n'
    + 'A     B  C                                      \n'
    t.equals(theTable, expected, 'The table was built correctly')
    t.end()
})
